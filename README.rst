role-debian-apt
===============

.. _Ansible: http://docs.ansible.com

An Ansible_ role that manages apt repositories. The role will not work on
distributions other than Debian GNU/Linux.
    
Requirements
------------

* Ansible_ 2 or higher

Installation
------------

There are two possibilities how to install the role. Choose one that suits you
best.

#. Copy the role into the ``roles`` subdirectory: ::

     cd YOUR_PROJECT
     mkdir -p roles/debian-apt
     wget -O - https://gitlab.com/tomaskadlec/ansible-role-debian-apt/repository/archive.tar.bz2 \
       | tar xjf - --wildcards -C roles/debian-apt --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p roles
     git submodule add git@gitlab.com:tomaskadlec/ansible-role-debian-apt.git roles/debian-apt

Configuration
-------------

``apt.cache_valid_time`` 
    Time while apt cache is considered valid. Unit is second. It defaults to 1
    day (e.g. 86400 seconds).

``apt.repositories``
    A dict containing apt repository definitions. Each entry consists of ``key``
    attribute (a GPG key) and ``repository``. 

An example configuration follows. ::

    apt:
        cache_valid_time: '{{24 * 60 * 60}}'
        repositories:
            ceph:
                key: 'https://download.ceph.com/keys/release.asc'
                repository: 'deb http://download.ceph.com/debian/ {{ansible_distribution_release}} main' 
            docker:
                key: 'https://yum.dockerproject.org/gpg'
                repository: 'deb https://apt.dockerproject.org/repo/ debian-{{ansible_distribution_release}} main'
    
Tags
----

The role defines tag ``apt``. All tasks are labeled with this tag.

Usage
-----

Refer to the role in your playbook. Tasks will be executed only if host
distribution is Debian. Please note that ``apt update`` (or ``apt-get update``)
will be executed if at least one of the following conditions is met:

* Cache is older then configured lifetime.
* A repository has been added or changed during the current run.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
