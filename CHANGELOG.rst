Change Log
==========

All notable changes to this project will be documented in this file [#]_.


1.0.0 - 2017-02-18
------------------

Added
~~~~~

* Change log (`CHANGELOG <CHANGELOG.rst>`_)
* Project description (`README <README.rst>`_)
* MIT License (`LICENSE <LICENSE>`_)
* Default configuration
* Tasks to manage apt repositories

.. Changed
.. ~~~~~~~
.. 
.. Fixed
.. ~~~~~
.. 
.. Removed
.. ~~~~~~~

.. _`Keep a Changelog`: http://keepachangelog.com/
.. _`Semantic Versioning`: http://semver.org/

.. [#] The format is based on `Keep a Changelog`_ and this project adheres to
   `Semantic Versioning`_.

.. vim: spelllang=cs spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
